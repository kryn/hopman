Hopman  is a  graphics tool  which displays  removable  partitions  currently
plugged  into the computer. It  displays also the  partition's label  if any,
and  the mountpoint,  if mounted.  On click, the user can  mount,  unmount or
open the partition's main directory in a file-manager or a terminal-emulator.
Hopman does not perform  mount, umount or open  by itself. Instead  a list of
command lines  must be provided in the  configuration file  to specify how to
perform these actions  and Hopman  invoques  these command lines. The command
lines specified in the default configuration file match the Xfce4 desktop:
for mount: pmount, for unmount: pumount, terminal emulator: xfce4-terminal,
file-manager: thunar.
Hopman uses the GTK+-2 graphics library.
Are considered as removable  the partitions of disks tagged as  "hotplug"  by
the kernel. The author doesn't know how the kernel decides wether a device is
hotpluggable or not  but this works with USB memory sticks of small or medium
capacity.  A future version of Hopman will give a better control  on how this
decision is made.
The key feature of Hopman is the mere absence of dependencies. The only ones
are
	a Linux kernel version higher or equal to 2.6,
	a C library,
	the graphics library (ie GTK-+2).
In particular, it doesn't depend on Gvfs, Dbus, and, of course, Systemd.
While Hopman does not depend on  the presence of a hotplugger, it relies on a
dynamic  creation/deletion  of device special files in /dev,  and, therefore,
it would not be  comfortable to use with a static /dev.  The dynamic creation
and removal of device files  do not require a hotplugger  and are better done
by the kernel itself if built with the DEVTMPFS feature,  but a hotplugger is
necessary to create the symbolic links in /dev/disk/by-label, on which Hopman
relies to know the labels of the partitions;  but Hopman is still functionnal
without the partitions' labels.
Hopman is a purely reactive process;  it sleeps all the time  except when the
kernel awakes it to report  a change in /dev, /dev/disk/by-label, or the file
/proc/self/mountinfo or when the user requests some action.  With the default
configuration,  the window of  Hopman  remains  invisible  when  there are no
hotplug partitions in the system; it pops up when one is inserted or out when
the last is removed.
