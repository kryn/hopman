/* See LICENSE file for copyright and license details. */ 
/*----------------------------------------------------------------------------
  This file provides the following functions:
  config_read(const char *filename) : parse and store configuration parameters
     Low-level getters:
  config_string(paramid_t) : return raw (string) parameter value
  config_int(paramid_t):     return integer parameter value as int
  config_bool(paramid_t):    return boolean parameter value as 0 or 1
     High level getters:
  config_pidfile(void) :     return pid file name ready to open
  config_helper(command_t) : return command line ready to invoke
  config_show(void) :        return menubar field list as bit flags
 ----------------------------------------------------------------------------*/
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <stdlib.h>
#include "hopman.h"

#define DECLARE_CONFIGBUF
#include "annex.h"

typedef struct
{
  const char *v;
  size_t l;
} varinfo_t;

static varinfo_t config_param[N_CONFIG_PARAMS];

const char *oldbuf; /* save value of config buffer in case it is realloc()ed */

/*======================== DECODE CONFIGURATION FILE ========================*/
/* The configuration parameters are declared static in this file; they will  */
/* be used by several functions in the same file.  The Default values only   */
/* exist in function config_read, and the modified values in the config file.*/
/* The varinfo_t type does not contain the text of the parameter values, but */
/* only pointers to it. We will calculate the size of the buffer necessary to*/
/* store all the parameter values, allocate it and make the pointers of the  */
/* varinfo_t point into it. First, the config file will be mmap()ed.         */
/* When the function returns, only the memory size needed to store the final */
/* values is retained.                                                       */

/*========= parse configuration file and store raw parameters' values =======*/
int config_read(const char *path)
{
  const char *parname[N_CONFIG_PARAMS] = { PARAM_NAMES };
  const char *default_param[N_CONFIG_PARAMS]= { PARAM_DEFAULTS };
  char *conf=NULL;
  char *b, *c, *d, *end;
  size_t size, len;
  int i;
    
  /*-------------------------- Set default values ---------------------------*/
  for(i=0; i<N_CONFIG_PARAMS; i++)
    {
      config_param[i].v = default_param[i];
      config_param[i].l = strlen(default_param[i]);
    }

  if( !path ) goto decoding_done;
  
  /*-------------------------- mmap the config file -------------------------*/
  {
    int fd, err;
    off_t osize;
    void *map;
    fd = open(path, O_RDONLY);
    if(fd == -1) return -1;
    osize = lseek(fd, 0, SEEK_END);
    if(osize == (off_t)-1)
      {
	 /* close fd while preserving errno */
	err = errno; close(fd); errno = err;
	return -1;
      }
    
    /* We mmap the config file with two bytes more than its size because we  */
    /* will append '\n' and '\0' at the end to secure the decoding loop. It  */
    /* won't affect the file because it was opened read-only; but we must    */
    /* mmap it with the PROT_READ and PROT_WRITE flags */
    size = (size_t)osize + 2;
    map = mmap( NULL, (size_t)size, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0 );
    err = errno; close(fd);  errno = err; /* close fd while preserving errno */
    if( map == MAP_FAILED ) return -1;
    conf = (char *)map;
    end  = conf+size;
    conf[size-2] = '\n';
    conf[size-1] = '\0';
  }

  /*------------------------ decode file content ----------------------------*/
  /* We have made sure our data is finished by an end-of-line and contained  */
  /* in a nulll-terminated string. We can now safely loop on lines by        */
  /* searching end-of-lines.                                                 */
  for( d=c=conf, strsep(&c, "\n");  c   ; d=c, strsep(&c, "\n") )
    {
      int p1, p2, p3;
      char *n, *v, *e;
      if( *d == '#' ) continue;
      p1=p2=p3=-1;
      sscanf(d, " %n%*[^= \t]%n = %n%*s", &p1, &p2, &p3);
      if(p3==-1) continue;
      n = d+p1; /* n points to begining of parameter name */
      v = d+p3; /* v points to beginning of parameter value */
      d[p2] = '\0'; /* put null byte at end of parameter name */
      for(b=n; *b; b++) *b = tolower(*b); /* lower case param name */
      for(i=0; i<N_CONFIG_PARAMS; i++)
	{
	  if( strcmp(parname[i], n) ) continue;
	  /* we have found an assignment instruction to a known parameter */
	  config_param[i].v = v;
      	  /* the parameter value now points into the memory map of the file */
	  /* properly terminate parameter value with a null byte */
	  for(e=c-1; e>v; e--)
	    {
	      if(*e==' ' || *e=='\t') *e = '\0';
	      else break;
	    }
	  config_param[i].l = strlen(v);
	}
    }
 decoding_done:
  /*  All the const char* of our array of varinfo_t now point to either some */
  /* location in the mmaped file  or some constant string  which exists only */
  /* in  this function.  We need to  store  all of them in the  long-lasting */
  /* configuration  buffer  before we  unmap  the file  and return  from the */
  /* function.                                                               */
  /*  The config buffer should already contain  username and user's home.    */
  
  /*---------- increase buffer size to store configuration data -------------*/
  /* Calculate size, including one byte per parameter string to store the \0 */
  len = configlen;
  for(i=0; i<N_CONFIG_PARAMS; i++) len += (config_param[i].l + 1);

  if( ! (c = (char *)realloc(configbuf, len)) )
    {
      if(conf)
	{
	  int err = errno;
	  munmap(conf, size);
	  errno = err;
	}
      return -1;
    }
  else oldbuf = configbuf = c; /* successfull realloc() */

  /*
    printf( "Buffer allocated from %p to %p (%u chars).\n",
    configbuf,configbuf+len-1,len);
  */
  
  /* copy variable values in buffer */
  for(b=configbuf+configlen, i=0; i<N_CONFIG_PARAMS; i++)
    {
      strcpy(b, config_param[i].v);
      config_param[i].v = b;
      b += (config_param[i].l + 1);
    }
  configlen = len;

  /*---------------------------- Unmap config file --------------------------*/
  if(conf) munmap(conf, size);

  /*
  for( b=configbuf, i=0; i<len; i++, b++ ) fputc( ((*b) ? *b : '\n'),  stdout);
  */

  return 0;
}

/* Recalculate string pointers if config buffer has been moved by realloc() */
static void reallocate(void)
{
  unsigned i;
  
  if(configbuf == oldbuf) return;

  for(i=0; i<N_CONFIG_PARAMS; i++)
    config_param[i].v += (configbuf-oldbuf);
  oldbuf = configbuf;
}

/*====================== parameter retrieval functions ======================*/
/* raw string */
const char *config_string(paramid_t param)
{
  if(configbuf != oldbuf) reallocate();
  return config_param[param].v;
}

/* boolean decoded as int */
int config_bool(paramid_t param)
{
  if(configbuf != oldbuf) reallocate();
  if(  !strcasecmp("true", config_param[param].v)  )   return 1;
  else if (  !strcasecmp("false", config_param[param].v)  )   return 0;
  else return -1;
}

/* integer decoded as int */
int config_int(paramid_t param)
{
  int i, n;
  if(configbuf != oldbuf) reallocate();
  n = sscanf( config_param[param].v, " %d ", &i );
  if(n==1) return i;
  else return -1;
}

/* pid file as string after substitution of escape sequences %[uh]*/
const char *config_pidfile(char *wbuf, size_t len)
{
  const char *c, *conf;
  char pc, *d, *end;

  end = wbuf+len;
  conf = config_string(PidFile);
  if(!conf) return NULL;

  for( c=conf, pc=0, d=wbuf;    *c && d<end;    pc=*c, c++, d++ )
    {
      const char *uname = username();
      const char *uhome = userhome();
      if(pc=='%') /* possible valid escape sequence */
	{
	  if( *c=='u' && uname )
	    {
	      d--;
	      strcpy(d, uname);
	      d += strlen(uname)-1;
	    }
	  else if( *c=='h' && uhome)
	    {
	      d--;
	      strcpy(d, uhome);
	      d += strlen(uhome)-1;
	    }
	  else*d = *c;
	}
      else *d = *c;
    }
  *d = '\0';
  return wbuf;
}

/* command helper as string after substitution of escape sequences %[lmnuh] */
const char *config_helper(command_t cmd, partinfo_t *p, char *wbuf, size_t len)
{
  static int debug = -1;
  paramid_t pi;
  const char *c, *conf;
  char pc, *d, *end;
  const char *uname = username();
  const char *uhome = userhome();

  if(debug == -1)
    {
      debug = config_bool(Debug);
      if(debug == -1) debug = 0;
    }

  end = wbuf + len;
  
  switch(cmd)
    {
    case Open_in_File_Manager:
      pi = FileManager;
      break;
    case Open_in_Terminal:
      pi = TerminalEmulator;
      break;
    case Mount:
      pi = MountHelper;
      break;
    case Unmount:
      pi=UmountHelper;
      break;
    case Eject:
      pi=EjectHelper;
      break;
    default: return NULL;
    }

  /* scan command line and substitute paramaters to escape sequences */
  conf = config_string(pi);

  if(debug)
    fprintf(stderr, "config_string = \"%s\"\n", conf);
  if(!conf) return NULL;

  for( c=conf, pc=0, d=wbuf;    *c && d<end;    pc=*c, c++, d++ )
    {
      if(pc=='%') /* possible valid escape sequence (hlmnu) */
	{
	  if( *c=='n')
	    {
	      d--;
	      strcpy(d, p->name);
	      if(debug) fprintf(stderr, "name: \"%s\"\n", p->name);
	      d += strlen(p->name)-1;
	    }
	  else if( *c=='l')
	    {
	      d--;
	      strcpy(d, p->label);
	      if(debug) fprintf(stderr, "label: \"%s\"\n", p->label);
	      d += strlen(p->label)-1;
	    }
	  else if( *c=='m' )
	    {
	      d--;
	      strcpy(d, p->mnt);
	      if(debug) fprintf(stderr, "mountpoint: \"%s\"\n", p->mnt);
	      d += strlen(p->mnt)-1;
	    }
	  else if( *c=='u' && uname)
	    {
	      d--;
	      if(debug) fprintf(stderr, "username: \"%s\"\n", uname);
	      strcpy(d, uname);
	      d += strlen(uname)-1;
	    }
	  else if( *c=='h' && uhome)
	    {
	      d--;
	      if(debug) fprintf(stderr, "uhome: \"%s\"\n", uhome);
	      strcpy(d, uhome);
	      d += strlen(uhome)-1;
	    }
	  else*d = *c;
	}
      else *d = *c;
    }
  *d = '\0';
  return wbuf;
}

/* return display configuration as bit flags */
unsigned config_show(void)
{
  unsigned flags = 0;
  
  if( config_bool(ShowName)    ==   1  )  flags |= SHOWNAME;
  if( config_bool(ShowLabel)    ==  1  )  flags |= SHOWLABEL;
  if( config_bool(ShowFstype)   ==  1  )  flags |= SHOWFSTYPE;
  if( config_bool(ShowMountPoint) == 1 )  flags |= SHOWMNT;
  if( config_bool(Sticky)      ==   1  )  flags |= STICKY;
  if( config_bool(AutoHide)   ==    1  )  flags |= AUTOHIDE;
  return flags;
}
