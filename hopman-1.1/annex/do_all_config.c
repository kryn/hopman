#include <stdio.h>
#include<linux/limits.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include "hopman.h"
#include "annex.h"

static int isregular(const char *path)
{
  struct stat buf;
  int rc;
  rc = stat(path, &buf);
  if(rc)
    {
      fprintf( stderr, _("%s cannot stat %s: %s\n"), progname, path,
	       strerror(errno) );
      return 0;
    }

  if ( (buf.st_mode & S_IFMT) == S_IFREG ) return 1;
  
  fprintf( stderr, _("%s: %s is not a regular file.\n"), progname,
	   path ); 
  return 0;
}
    
/* Invoke userinfo(), config_read() and newpid() */
void do_all_config(void)
{
  const char *rcname = "hopmanrc";
  char filename[NAME_MAX+1];
  const char *h;
  int rc;
  
  if( userinfo_init() )
    {
      fprintf(stderr, _("%s cannot retrieve username and home: %s\n"),
	      progname, strerror(errno));
    }
  
  h = userhome();

  /*----------- Try to find and read user's configuration file --------------*/
  rc = -1;
  if(h)
    {
      strcpy(filename, h);
      strcat(filename, "/.hopmanrc");
      if(isregular(filename))
	{
	  rc = config_read(filename);
	  if(rc) fprintf( stderr, _("%s cannot process %s: %s\n"), progname,
			  filename, strerror(errno) );
	  else fprintf( stderr, _("%s using configuration from %s.\n"), progname,
			filename );
	}
      else rc = -1;
    }

  /*----------- If no user's configuration file, try default one ------------*/
  if(rc)
    {
      strcpy(filename, "/etc/default/hopmanrc");
      if(isregular(filename))
	{
	  rc = config_read(filename);
	  if(rc) fprintf( stderr, _("%s cannot process %s: %s\n"), progname,
			  filename, strerror(errno) );
	  else fprintf( stderr, _("%s using configuration from %s.\n"), progname,
			filename );
	}
      else rc = -1;
    }
  /*-------- If no configuration file at all, try built-in defaults ---------*/
  if(rc)
    {
      rc = config_read(NULL);
      if(rc)
	fprintf( stderr,
		 _("%s internal error with built-in default configuration: %s\n"),
		 progname, strerror(errno) );
      else fprintf( stderr, _("%s: using built-in default configuration.\n"),
		    progname );
    }
  
  /*-------------------- If everything failed, then exit. -------------------*/
  if(rc) exit(EXIT_FAILURE);
  
  newpid();
}
