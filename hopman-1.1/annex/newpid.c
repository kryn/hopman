#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include "hopman.h"
#include "annex.h"

void cleanpid(int pid, void *pdata)
{
  const char *fn = (const char *)pdata;
  unlink( fn );
  return;
}
void newpid(void)
{
  FILE *pf;
  int k, l, n, rc;
  struct timespec milisec;
  const char *fn;
  pid_t pid, mypid;
  static char buf[NAME_MAX+1];

  milisec.tv_sec = 0;
  milisec.tv_nsec = 1000000;
  
  fn = config_pidfile(buf, NAME_MAX+1);
  if( !fn )
    {
      fprintf(stderr, _("%s: no pid-file configured.\n"), progname);
      return;
    }

  mypid = getpid();
  
  /* try hard to kill previous instance */
  for( k=rc=0; k<3 && (pf=fopen(fn, "r"));  k++)
    {
      n = fscanf(pf, " %d ", &l);
      fclose(pf);
      if(n==1 && !rc)
	{
	  pid = l;
	  if(pid < 2) break;
	  rc = kill(pid, SIGKILL);
	}
      nanosleep(&milisec, NULL);
    }

  /*---- Make sure all directories on the path exist before opening file ----*/
  /* remember fn points to buf. Count slashes */
  for(l=0; buf[l] == '/'; l++); /* skip leading slashes */
  for(k=l, n=0; buf[k]; k++) if(buf[k] == '/') n++;
  {
    char *slash[n], *c;
    struct stat stb;
    for(k=0, c=buf+l; *c; c++) /* record slash positions */
      {
	if(*c=='/')
	  {
	    slash[k] = c;
	    k++;
	  }
      }
    for(k=0; k<n; k++) *slash[k]='\0'; /* replace all '/' by '\0' */
    for(k=0; k<n; k++)
      {
	/* if directory doesn't exist, create it */
	if( stat(buf, &stb) )
	  {
	    if(errno == ENOENT)
	      {
		if( mkdir(buf, 0755) ) goto error;
	      }
	    else goto error;
	  }
	*slash[k] = '/'; /* put slash back */
      }
  }
  pf = fopen(fn, "w");
  if(!pf) goto error;
  fprintf(pf, "%u\n", mypid);
  fclose(pf);
  on_exit(cleanpid, (void *)fn); /* fn points to buf which is static */
  return;

 error:
  fprintf(stderr, _("%s can't create pid file: %s\n"), progname, strerror(errno));
  return;
}
