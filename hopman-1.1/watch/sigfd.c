#include <sys/signalfd.h>
#include <libgen.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include "hopman.h"

#define DECLARE_OLDSET
#include "watch.h"

int signalinit(void)
{
  sigset_t sigset;
  int sfd;

  /*------------------------ initialize signal handling ---------------------*/
  sigemptyset( &sigset );
  sigaddset( &sigset, SIGCHLD); /* need to reap a child */
  sigaddset( &sigset, SIGHUP);  /* show the window if hidden */
  sigaddset( &sigset, SIGTERM); /* gracefully exit */
  sigaddset( &sigset, SIGINT);  /* gracefully exit */
  /* Block all these signals. We will read them by signalfd */
  if (sigprocmask( SIG_BLOCK, &sigset, &oldset )) /* oldset is global */
    {
      fprintf(stderr, _("%s error found by sigprocmask: %s\n"),
	      progname, strerror(errno));
      exit(EXIT_FAILURE);
    }
  sfd = signalfd(-1, &sigset, SFD_CLOEXEC); /* route signals through sfd */

  if(sfd < 0)
    {
      fprintf(stderr, _("%s error found by signalfd: %s\n"),
	      progname, strerror(errno));
      exit(EXIT_FAILURE);
    }

  return sfd;
}

void signal_read(int sfd)
{
  int n;
  pid_t pid;
  struct signalfd_siginfo sifo;

  do { n = read(sfd, &sifo, sizeof(sifo)); } while (n<1 && errno==EINTR);
  if( n != sizeof(sifo) )
    {
      fprintf(stderr, _("%s error reading signal info: %s\n"), strerror(errno));
      exit(EXIT_FAILURE);
    }
  
  switch(sifo.ssi_signo)
    {
    case SIGCHLD:
      /* one of our external commands is terminated */
      do
	{
	  int wstatus;
	  pid = waitpid( -1, &wstatus, WNOHANG );
	  if(pid>0) command_finished(pid, wstatus);
	} while( pid>0 );
      break;
    case SIGHUP:
      ui_set_visible(-1); /* toggle visibility */
      break;
    case SIGTERM:
    case SIGINT:
      exit(EXIT_SUCCESS); /* we have been killed, exit gracefully. */
      break;
    }
}
