/* See LICENSE file for copyright and license details. */ 
/* Check if a device is a partition and its disk is removable */
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <string.h>
#include <errno.h>
#include "watch.h"

int hotplug_partition(const char *name)
{
  char namebuf[NAME_MAX+1], *pathname, c;
  int fd, devfd; /* file descriptors */
  struct stat buf;
  int removable = 0;
  
  if( !(pathname=devpath(name, namebuf)) )
    {
      fprintf(stderr, _("%s cannot find sys path for %s.\n"), progname, name);
      goto close0;
    }
  
  /* open directory pointed to by pathname */ 
  if( (devfd=open(pathname, O_RDONLY)) < 0 )
    {
      fprintf(stderr, _("%s cannot open %s: %s.\n"),
	      progname, name, strerror(errno));
      goto close0;
    }

  /* check the directory contains a file named "partition" */
  if(fstatat( devfd, "partition", &buf, 0)) goto close1;

  /* open file "removable" in parent directory */
  if( (fd=openat(devfd, "../removable", O_RDONLY)) < 0 )
    {
      fprintf(stderr,
	      _("%s: can't find in /sys if device of %s is removable.\n"),
	      progname, name);
      goto close1;
    }

  /* read 1st character of this file */
  if(read(fd, &c, 1) != 1)
    {
      fprintf(stderr,
	      _("%s cannot read if parent device of %s is removable: %s.\n"),
	      progname, name, strerror(errno));
      goto close2;
    }

  removable = (c =='1');
  
 close2: close(fd);
 close1: close(devfd);
 close0: return removable;
}
