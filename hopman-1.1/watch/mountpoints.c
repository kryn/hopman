#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>
#include <stdlib.h>
#include <limits.h>
#include "hopman.h"
#include "watch.h"
  static FILE *pf = NULL;
  static int debug = -1;
/* On 1st call open mountinfo, on all calls return the file descriptor */
int mountinit(void)
{
  if(debug == -1)
    {
      debug = config_bool(Debug);
      if(debug == -1) debug = 0;
    }
  
  if(!pf)
    {
      pf=fopen("/proc/self/mountinfo", "r");
      if(!pf)
	{
	  switch(errno)
	    {
	    case ENOMEM:
	      fprintf(stderr, _("%s error opening /proc/self/mountinfo: %s\n"),
		      progname, strerror(errno));
	      break;
	    default:
	      fprintf( stderr, _("%s: %s: /proc/self/mountinfo.\n"),
		       progname, strerror(errno) );
	      exit(EXIT_FAILURE);
	    }
	}
    }
  return fileno(pf);
}

void mountpoints(void)
{
  /* read /proc/self/mountinfo and decode */
  /* it is impossible to stat the size of this file, nor to report
     its size by invoking lseek(fd, 0, SEEK_END). The first method
     gives 0 and the second -1. We resort to reading it line by line
     and the easiest for this is stdio. */

  char line[300];
  partlist_t P;
  unsigned anychange;

  P = partition_get_list();
  if(!P.np) return;

  /*--------------- open on first call, rewind on other calls ---------------*/
  if(!pf) mountinit();
  else fseek(pf, 0, SEEK_SET);

  /*----- Dynamically allocate two arrays in the stack and update data ------*/
  {
    unsigned index;
    unsigned nmounts[P.np];
    char changed[P.np];

    /* reset mount counters and modification flags */
    for(index=0; index<P.np; index++) changed[index] = nmounts[index] = 0;
    anychange = 0;
    
    /* decode every line of /proc/self/mountinfo */
    while( fgets(line, sizeof(line), pf) )
      {
	int mp[2] = {0,0}, fs[2], df[2] = {0,0};
	char *mtpt, *fstp, *devf, *partitionname;
	/* get start and end of mountpoint path string and device file name */
	sscanf(line, "%*s %*s %*s %*s %n%*s%n%*[^-]- %n%*s%n %n%*s%n",
	       mp, mp+1, fs, fs+1, df, df+1);
	mtpt = line+mp[0];
	fstp = line+fs[0];
	devf = line+df[0];
	line[mp[1]] = line[fs[1]] = line[df[1]] = '\0';
	partitionname=basename(devf);
	/* find if the partition is in our list */
	index = partition_by_name(partitionname);
	if( index < P.np )
	  {
	    /* yes, here's the partition */
	    nmounts[index]++; /* increment the number of partition's mounts */
	    if( nmounts[index]==1 && strcmp(mtpt, P.p[index]->mnt) )
	      {
		/* It is the 1st mountpoint of the partition and it changed */
		fprintf(stderr, "1st mountpoint changed on partition #%d\n",
			index);
		strncpy( P.p[index]->fstype, fstp, PM_FSTYPE_LEN);
		strncpy(P.p[index]->mnt, mtpt, PM_MNT_LEN);
		changed[index] = 1;
		anychange = 1;
	      }
	  }
      }

    /* Tag all partitions for which the number of mountpoints has changed and
       nullify the mountpoint string when the number of mountpoints is 0 */
    for(index=0; index<P.np; index++)
      {
	if( P.p[index]->nmounts != nmounts[index] )
	  {
	    P.p[index]->nmounts = nmounts[index];
	    changed[index] = 1;
	    anychange = 1;
	    if( nmounts[index]==0 ) P.p[index]->mnt[0] = '\0';
	  }
      }

    /* invoke ui_update for all changed partitions */
    for( index=0; index<P.np; index ++ )
      {
	if( changed[index] ) ui_update( P.p[index] );
      }
    /* all done */
  }

  if(debug && anychange) partition_print();

  return;
}
