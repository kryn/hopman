#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "watch.h"

/*====== A subprogram like popen() but with diferences explained below ======*/
/* The pipe is to read in the current process and connected to stderr in the */
/* new one. The intent is to report error messages. Plus pid is returned.    */
/* If dirpath is an absolute path, chdir to it prior to excuting command.    */
FILE *epopen( const char *command,  pid_t *pid , const char *dirpath)
{
  int pipefd[2];
  pid_t p_id;
  FILE *pf;
  int rc;

  /*--------------------------- Create the pipe -----------------------------*/
  if(pipe(pipefd))
    {
      *pid = -1;
      return NULL;
    }

  /*------------------------------------ fork -------------------------------*/
  p_id = fork();
  switch(p_id)
    {
    case 0: /* the child */
      close(pipefd[0]);
      close(STDERR_FILENO);
      dup2(pipefd[1], STDERR_FILENO);
      close(pipefd[1]);
      if( *dirpath == '/' ) chdir( dirpath );
      /* restore original mask */
      if( sigprocmask(SIG_SETMASK, &oldset, NULL) )
	fprintf( stderr, _("%s: error detected by sigprocmask(): %s\n"),
		 progname, strerror(errno) );
      execl( "/bin/sh", "sh", "-c", command, (char *)NULL );
      fprintf( stderr, _("%s: error invoking /bin/sh: %s\n"),
	       progname, strerror(errno) );
      exit(EXIT_FAILURE); /* reminder: we are the child */
    case -1: /* fork() failed ! */
      rc = errno;
      close(pipefd[0]);
      close(pipefd[1]);
      errno = rc;
      *pid = -1;
      return NULL;
    default: /* the parent */
      close(pipefd[1]);
      pf = fdopen(pipefd[0], "r");
      *pid = p_id;
      return pf; /* The caller must close the pipe after use */
    }
}
